// NtupleMaker includes
#include "NtupleMakerAlg.h"
#include "PathResolver/PathResolver.h"


#include <random>
#include <fstream>


//#include "xAODEventInfo/EventInfo.h"



std::string getFileContents(std::string  filepath)
{
  std::ifstream File(filepath);
  std::string Lines = "";        //All lines
  if(File)                      //Check if everything is good
  {
    while(File.good ())
    {
      std::string TempLine;                  //Temp line
      std::getline(File , TempLine);         //Get temp line
      TempLine += "\n";                      //Add newline character
      Lines += TempLine;                     //Add newline
    }
    return Lines;
  }
  else                           //Return error
  {
    return "ERROR File does not exist.";
  }
}

NtupleMakerAlg::NtupleMakerAlg( const std::string& name, ISvcLocator* pSvcLocator ) : AthAnalysisAlgorithm( name, pSvcLocator ){

  //declareProperty( "Property", m_nProperty = 0, "My Example Integer Property" ); //example property declaration
  declareProperty("ascii_file", m_art_file);

}


NtupleMakerAlg::~NtupleMakerAlg() {}


StatusCode NtupleMakerAlg::initialize() {
  ATH_MSG_INFO ("Initializing " << name() << "...");
  //
  //This is called once, before the start of the event loop
  //Retrieves of tools you have configured in the joboptions go here
  //

  //HERE IS AN EXAMPLE
  //We will create a histogram and a ttree and register them to the histsvc
  //Remember to configure the histsvc stream in the joboptions
  //
  //m_myHist = new TH1D("myHist","myHist",100,0,100);
  //CHECK( histSvc()->regHist("/MYSTREAM/myHist", m_myHist) ); //registers histogram to output stream
  //m_myTree = new TTree("myTree","myTree");
  //CHECK( histSvc()->regTree("/MYSTREAM/SubDirectory/myTree", m_myTree) ); //registers tree to output stream inside a sub-directory


  // get the cow ascii art 
  std::string cow_file = PathResolverFindCalibFile(m_art_file);
  std::cout << "Found cow file at : " << cow_file << std::endl;
  m_ascii_art = getFileContents(cow_file);


  // We always start with printing a cow.
  // That's how we have always done things here at ATLAS UK!
  // How dare you question the relevance of this! 
  std::cout << m_ascii_art << std::endl;



  return StatusCode::SUCCESS;
}

StatusCode NtupleMakerAlg::finalize() {
  ATH_MSG_INFO ("Finalizing " << name() << "...");
  //
  //Things that happen once at the end of the event loop go here
  //


  return StatusCode::SUCCESS;
}




int NtupleMakerAlg::get_random_number( )
{
  // Return a number from [0,1000] drawn from a uniform distribution.
  static std::default_random_engine e{};
  static std::uniform_int_distribution<int> d{0, 1000};
  return d(e);
}





StatusCode NtupleMakerAlg::execute() {  
  ATH_MSG_DEBUG ("Executing " << name() << "...");
  setFilterPassed(false); //optional: start with algorithm not passed

  //
  //Your main analysis code goes here
  //If you will use this algorithm to perform event skimming, you
  //should ensure the setFilterPassed method is called
  //If never called, the algorithm is assumed to have 'passed' by default
  //

  //HERE IS AN EXAMPLE
  //const xAOD::EventInfo* ei = 0;
  //CHECK( evtStore()->retrieve( ei , "EventInfo" ) );
  //ATH_MSG_INFO("eventNumber=" << ei->eventNumber() );
  //m_myHist->Fill( ei->averageInteractionsPerCrossing() ); //fill mu into histogram


  // Do something with your event. 
  if(get_random_number() < 1){
    std::cout << "Moo moooo!" << std::endl;
  }
  else if(get_random_number() < 100){
    std::cout << m_ascii_art <<  std::endl;
  }
  else{
    std::cout << "Moo!" << std::endl;
  }
  
  



  setFilterPassed(true); //if got here, assume that means algorithm passed
  return StatusCode::SUCCESS;
}

StatusCode NtupleMakerAlg::beginInputFile() { 
  //
  //This method is called at the start of each input file, even if
  //the input file contains no events. Accumulate metadata information here
  //

  //example of retrieval of CutBookkeepers: (remember you will need to include the necessary header files and use statements in requirements file)
  // const xAOD::CutBookkeeperContainer* bks = 0;
  // CHECK( inputMetaStore()->retrieve(bks, "CutBookkeepers") );

  //example of IOVMetaData retrieval (see https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/AthAnalysisBase#How_to_access_file_metadata_in_C)
  //float beamEnergy(0); CHECK( retrieveMetadata("/TagInfo","beam_energy",beamEnergy) );
  //std::vector<float> bunchPattern; CHECK( retrieveMetadata("/Digitiation/Parameters","BeamIntensityPattern",bunchPattern) );



  return StatusCode::SUCCESS;
}


